public class Producto {
  private long codigo;
  public String nombre;
  private float precioCosto;
  private int porcentajeGanancia;
  private int iva = 21;
  public float precioVenta;

  public void setCodigo(long codigo) {
    this.codigo = codigo;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public void setPrecioCosto(float precioCosto) {
    this.precioCosto = precioCosto;
  }

  public void setPorcentajeGanancia(int porcentajeGanancia) {
    this.porcentajeGanancia = porcentajeGanancia;
  }

  
  
  public long getCodigo(){
    return codigo;
  }

  public String getNombre(){
    return nombre;
  }

  public float getPrecioCosto(){
    return precioCosto;
  }

  public float getPorcentajeGanancia(){
    return porcentajeGanancia;
  }

  public int getIva(){
    return iva;
  }

  public float getPrecioVenta(){
    return precioVenta;
  }

  public void mostrarInfoRelativa(){
    System.out.println("--------------");
    System.out.println("Nombre del producto: "+nombre);
    System.out.println("Codigo del producto: "+codigo);
    System.out.println("Precio de costo: "+precioCosto);
    System.out.println("Porcentaje de ganancia: "+porcentajeGanancia+"%");
    System.out.println("IVA: "+iva+"%");
    System.out.println("Precio de venta: "+precioVenta);
    System.out.println("--------------");
  }

  public void calcularPrecioVenta(){
    float precioTotal = precioCosto + (this.precioCosto * this.porcentajeGanancia)/100;
    this.precioVenta = precioCosto + (this.iva * precioTotal)/100; 
  }
}