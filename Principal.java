public class Principal {
  public static void main(String [] args){
    Producto producto1 = new Producto();
    Producto producto2 = new Producto();

    producto1.setCodigo(15);
    producto1.setNombre("Azucar");
    producto1.setPrecioCosto(50);
    producto1.setPorcentajeGanancia(25);
    producto1.calcularPrecioVenta();
    producto1.mostrarInfoRelativa();


    producto2.setCodigo(024);
    producto2.setNombre("Leche");
    producto2.setPrecioCosto(90);
    producto2.setPorcentajeGanancia(37);
    producto2.calcularPrecioVenta();
    producto2.mostrarInfoRelativa();
    

    if (producto1.precioVenta > producto2.precioVenta){
      System.out.println(producto1.nombre+" es mas caro");
    }else 
      System.out.println(producto2.nombre+" es mas caro");
    
  }
}